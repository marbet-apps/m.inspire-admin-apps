export class User {
  id: number;
  username: string;
  password: string;
  first_name: string;
  last_name: string;
  token?: string;

  get name() {
    return this.first_name + ' ' + this.last_name;
  }
}
