import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../auth.service';
import { NotificationService } from '../../shared/notifications/notifications.service';
import { TranslateService } from '@ngx-translate/core';
import { LoaderService } from '../../shared/loader/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  returnUrl: string;
  submitted = false;
  error = '';
  formErrors = {
    email: '',
    password: ''
  };

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService, private route: ActivatedRoute,
              private router: Router, private notificationService: NotificationService, private translate: TranslateService,
              private loaderService: LoaderService
  ) {
    translate.setDefaultLang(this.authenticationService.getLanguage());
  }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]
      ],
      password: ['', [
        // Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(4),
        Validators.maxLength(25)
      ]
      ],
    });

    if (this.authenticationService.isLoggedIn()) {
      this.router.navigate(['/admin']);
      return;
    }

    // reset login
    this.authenticationService.logout();

    // get return url from route parameters or default to '/dashboard'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/admin';

    this.loginForm.valueChanges.subscribe(() => {
      if ((this.loginForm.controls.email.value === '') ||
        (this.loginForm.controls.password.value === '')) {
        this.error = '';
      }
    });
  }

  useLanguage(language: string) {
    this.translate.use(language);
    this.authenticationService.setLanguage(language);
  }

  // convenience getter for easy access to form fields
  get formControl() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.error = '';
    if (!this.loginForm.valid) {
      this.notificationService.error('login.EnterAllInfo', true);
      return;
    }
    this.loaderService.show(true);
    this.authenticationService.login(this.loginForm.value, 1).subscribe((res) => {
      this.loaderService.hide(true);
      if (res.code === '705' && res.data != null) {
        this.router.navigate([this.returnUrl]);
      } else if (res.code === '500' || res.code === '400') {
        console.log('Error' + res.message);
        this.notificationService.error(res.message, false);
      } else {
        console.log('Error' + res.message);
        this.notificationService.error('login.Msg' + res.code, true);
      }

    }, (err) => {
      this.error = '';
      this.loaderService.hide(true);
      this.notificationService.error(err.code + ' - ' + err.message, false);
    });
  }

}
