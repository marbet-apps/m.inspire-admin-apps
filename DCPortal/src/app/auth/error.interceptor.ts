import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificationService } from './../shared/notifications/notifications.service';
import { AuthenticationService } from './auth.service';
import { LoaderService } from '../shared/loader/loader.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  isUnitTests = false;

  constructor(
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private loaderService: LoaderService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      this.loaderService.hide(true);
      if (err && err.status === 401) {
        this.notificationService.warn('login.SessionExpired', true);
        this.authenticationService.logout();
        if (location.pathname !== '/login' && !this.isUnitTests) {
          location.assign('/login');
        }
        return from<any>(new Promise(resolve => {
          resolve(true);
        }));
      }
      return throwError(err);
    }));
  }
}
