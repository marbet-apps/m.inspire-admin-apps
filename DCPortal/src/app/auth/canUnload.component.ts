import { HostListener } from '@angular/core';

export abstract class ComponentCanUnload {
  isUnitTestProcess = false;
  abstract canRefresh(): boolean;

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (!this.isUnitTestProcess && !this.canRefresh()) {
      $event.returnValue = 'You are about to leave this page with unsaved data.';
      return 'You are about to leave this page with unsaved data.';
    }
  }
}
