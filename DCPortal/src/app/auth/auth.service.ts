import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { appConstants } from '../app.constants';

import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements OnDestroy {
  privileges = [];
  operationModelSubscription: any;


  constructor(private http: HttpClient) {
  }

  get currentUser() {
    return JSON.parse(localStorage.getItem('currentUser') || '{}');
  }

  getUserFullName(user: any) {
    let preparedBy = '';
    if (user) {
      if (user.First_Name && user.Last_Name) {
        preparedBy = user.First_Name + ' ' + user.Last_Name;
      } else if (user.First_Name && !user.Last_Name) {
        preparedBy = user.First_Name;
      } else {
        preparedBy = user.Last_Name;
      }
    }
    return preparedBy;
  }


  ngOnDestroy() {
    this.operationModelSubscription.unsubscribe();
  }

  login(userData, flag): Observable<any> {
    return this.http.post<any>(appConstants.baseUrl + appConstants.api.auth.login,
      JSON.stringify({
        Email: userData.email,
        Password: userData.password,
        UserType: flag
      }), {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        observe: 'response'
      }).pipe(map((res: any) => {

        const token = res.headers.get('authorization');
        const user = (res && res.body.code === 705) ? res.body.data : {};
        // login successful if there's a jwt token in the response
        if (user && res.body.data) {
          user.token = token;
          user.data = res.body.data;
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          user.fullName = this.getUserFullName(res.body.data);
         // user.photoURL = 'assets/Images/profile.jpg';
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return res.body;
      }));
  }

  isLoggedIn() {
    return localStorage.getItem('currentUser');
  }

  getLanguage() {
    return localStorage.getItem('lang') || 'en';
  }
  setLanguage(lang: string) {
    localStorage.setItem('lang', lang);
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  isValidSession() {
    return this.http.get<any>(appConstants.baseUrl + appConstants.api.auth.dashboard);
  }

}
