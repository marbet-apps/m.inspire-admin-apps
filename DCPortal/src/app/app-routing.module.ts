import { Routes } from '@angular/router';
import { AuthGuard } from '../app/auth/auth.guard';
import { LoginComponent } from './auth/login/login.component';

export const AppRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: './admin/admin.module#AdminModule',
    runGuardsAndResolvers : 'always'
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

