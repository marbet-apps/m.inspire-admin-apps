import { environment } from '../environments/environment';

export const appConstants = {
  baseUrl: environment.apiHost + '/api',
  api: {
    version: '',
    auth: {
      version: '',
      login: '/AdminSecure/login',
      dashboard: '/dashboard',
      logout: '/logout'
    },
    admin: {
      version: '',
      path: '/AdminSecure',
      users: {
        version: '',
        getAll: '/GetAllUser',
        getDetail: '/UserDetail',
        register: '/Register',
        updateUser: '/UpdateUser',
        generatePassword: '/GeneratePassword',
        deleteUser: '/DeleteUser',
      },
      rfp: {
        version: '',
        getRFP:'/GetRFPMiceRequestWithProposal'
      }
    },
    lookup: {
      version: '',
      path: '/LookUp',
      list: {
        country: '/CountryList',
        subscriptionType: '/SubscriptionType',
        requestStatus: '/RequestStatusList',
      }
    }
  },
  default: {
    caseSetName: 'Case Set 1',
    defaultTimezone: 'GMT-8', // without day light saving timezone
    defaultDLSTimezone: 'GMT-7', // day light saving timezone
    defaultTimezoneName: 'America/Los_Angeles',
    defaultTimezoneCode: 'PST',
    defaultHhMmSs: '00:00:00',
    defaultEndDayHhMmSs: '23:59:59',
    dateFormatWithHhMmSs: 'MM/DD/YYYY HH:mm:ss',
    dateFormatWithoutHhMmSs: 'MM/DD/YYYY',
    dayLightSavingTimeConfig: {
      numberOfWeekStart: 2, // second week
      startMonth: 2, // start month - march
      numberOfWeekEnd: 1, // first week
      endMonth: 10, // end month - november
      day: 0 // sunday
    }
  },
  OPERATION_NOT_ALLOWED: 'OPERATION_NOT_ALLOWED',
  httpCodes: {
    insufficient_param: '607',
    success: '701',
    passwordGenerate: '701',
    userAdded: '701',
    userUpdated: '703',
    userAlreadyExist: '612',
    recordFound: '704',
    noRecords: '609'
  },
  statusCodes: {
    bad_request: 400,
    unauthorized: 401,
    forbidden: 403,
    not_found: 404,
    internal_server_err: 500,
    created: 201,
    success: 200,
    already_exist: 409,
    pdfFileCorrupted: 412
  },
  activeStatusOptions: [{
    value: 'All',
    display: 'All'
  }, {
    value: 'Active',
    display: 'Active'
  }, {
    value: 'InActive',
    display: 'InActive'
  }],
};

export const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

export enum TableAction {
  View = 'view',
  Add = 'add',
  Edit = 'edit',
  Delete = 'delete',
}

export enum ActiveStatus {
  Active = 'Active',
  InActive = 'InActive',
}

export const BsDatepickerConfig = {
  dateInputFormat: 'MM/DD/YYYY',
  containerClass: 'theme-white',
  showWeekNumbers: false,
  class: 'modal-dialog-centered'
};

export const DateFormats = {
  standardDateFormat: 'MM/DD/YYYY',
  tooltipDateFormat: 'MM/DD/YYYY HH:mm A'
};

export const monthList = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December'
};

export class Notification {
  type: NotificationType;
  message: string;
}

export enum NotificationType {
  Success,
  Error,
  Info,
  Warning
}
