import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { AuthenticationService } from '../../auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  isOpen = false;
  currentUser: any;
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) {
      return;
    }

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.isOpen = false;
    }
  }


  constructor(private elementRef: ElementRef, private authService: AuthenticationService,
              private router: Router) {
                this.currentUser = this.authService.currentUser;
              }


  ngOnInit() {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  navigateToLink(route: string) {
    this.isOpen = false;
    this.router.navigate([route]);
  }

}
