import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-sidemenu-item',
  templateUrl: './sidemenu-item.component.html',
  styleUrls: ['./sidemenu-item.component.scss']
})
export class SidemenuItemComponent implements OnInit {

  @Input() menu;
  @Input() paths;
  @Input() iconOnly: boolean;
  @Input() secondaryMenu = false;

  constructor(private router: Router) {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.setActiveTab(event.urlAfterRedirects);
        }
      });
  }

  ngOnInit() {
    this.paths.homePaths.isActive = true;
  }

  openLink() {
    this.menu.open = this.menu.open;
  }

  chechForChildMenu() {
    return (this.menu && this.menu.sub) ? true : false;
  }

  setActiveTab(url) {
    Object.keys(this.paths).forEach(key => {
      if (this.activetab(this.paths[key].data, url)) {
        this.paths[key].isActive = true;
      } else {
        this.paths[key].isActive = false;
      }
    });
  }

  activetab(tabnames, url) {
    const routeTabname = JSON.stringify(url);
    let answer = false;
    tabnames.forEach(tabname => {
      const currentTabName = new RegExp(JSON.stringify(tabname));
      if (currentTabName.test(routeTabname)) {
        answer = true;
      }
    });
    return answer;
  }

}
