import { BroadcastService } from './../broadcast/broadcast.service';
import {Component, OnInit} from '@angular/core';
import { appConstants } from '../../app.constants';

@Component({
  selector: 'app-error-403',
  templateUrl: './error.component.html'
})
export class ErrorComponent implements OnInit {
  constructor(private broadcastService: BroadcastService) {
  }

  ngOnInit() {
    
  }

  cancel() {
    this.broadcastService.broadcast(appConstants.OPERATION_NOT_ALLOWED, true)
  }
}
