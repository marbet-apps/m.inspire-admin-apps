import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { appConstants } from '../../app.constants';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export abstract class HttpBaseService {
  constructor(public httpClient: HttpClient) {

  }

  abstract prepareRequestJson(obj: any);

  abstract prepareSearchRequestJson(obj: any);

  get(methodUrl, params?: any) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    if (params) {
      return this.httpClient.get(completeApiUrl, { params });
    } else {
      return this.httpClient.get(completeApiUrl);
    }
  }

  downloadFile(methodUrl, methodtype?: string, queryString?: any, filterParams?: any) {
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      // remove last '&'
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { headers, responseType: 'blob', observe: 'response' };

    if (methodtype === 'POST') {
      const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
      return this.httpClient.post(completeApiUrl, filterParams, options).pipe(map((res: any) => {
        return this.handleDownloadFileResponse(res);
      }));
    } else {
      const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
      return this.httpClient.get(completeApiUrl, options).pipe(map((res: any) => {
        return this.handleDownloadFileResponse(res);
      }));
    }
  }

  handleDownloadFileResponse(res) {
    const contentDisposition = res.headers.get('Content-Disposition');
    const filename = contentDisposition.substring(contentDisposition.indexOf('=') + 1);
    return {
      res,
      filename
    };
  }

  create(methodUrl, object, getFullResponse?: boolean, queryString?: any) {
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      // remove last '&'
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    if (getFullResponse) {
      return this.httpClient.post(completeApiUrl, object, {
        headers: {
          'Content-Type': 'application/json'
        },
        observe: 'response'
      });
    }
    return this.httpClient.post(completeApiUrl, object, httpOptions);
  }

  fileupdate(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  update(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl) + object.id;
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  delete(methodUrl, objId?: string) {
    let completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    if (objId) {
      completeApiUrl = completeApiUrl + objId;
    }
    return this.httpClient.delete(completeApiUrl);
  }

  deleteWithMultipleIds(methodUrl, ids) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    let httpParams = new HttpParams();
    if (ids) {
      ids.forEach(id => {
        httpParams = httpParams.append('ids', id);
      });
    }
    return this.httpClient.delete(completeApiUrl, { params: httpParams });
  }

  deleteWithQueryParams(methodUrl: string, object?: any, queryString?: any) {
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    const httpOption = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: object
    };
    return this.httpClient.delete(completeApiUrl, httpOption);
  }

  deleteWithParams(methodUrl: string, object?: any) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    const httpOption = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: object
    };
    return this.httpClient.delete(completeApiUrl, httpOption);
  }

  post(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.post(completeApiUrl, object);
  }

  put(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  putWithParam(methodUrl, object, queryString?: any) {
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  postAndReturnFile(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.post(completeApiUrl, object, { responseType: 'blob', observe: 'response' });
  }

  getAndReturnFile(methodUrl, params) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    if (params) {
      return this.httpClient.get(completeApiUrl, { params, responseType: 'blob', observe: 'response' });
    } else {
      return this.httpClient.get(completeApiUrl, { responseType: 'blob', observe: 'response' });
    }
  }

  getCompleteApiUrl(methodUrl) {
    return appConstants.baseUrl + methodUrl;
  }

  updateWithUrl(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  list(methodUrl, methodtype?: string, queryString?: any, filterParams?: any) {
    // const prepareQueryStringObject = this.prepareQueryStringObject(queryString);
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      // remove last '&'
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }
    if (methodtype === 'POST') {
      return this.create(methodUrl, filterParams).pipe(map((res: any) => {
        return res;
      }));
    } else if (methodtype === 'PUT') {
      return this.put(methodUrl, filterParams).pipe(map((res: any) => {
        return res;
      }));
    } else {
      return this.get(methodUrl).pipe(map((res: any) => {
        return res;
      }));
    }
  }

  updateReorder(methodUrl, object) {
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    return this.httpClient.put(completeApiUrl, object, httpOptions);
  }

  prepareQueryStringObject(queryStringObj: any) {
    return {
      sort_column: queryStringObj.sort_column,
      sort_direction: queryStringObj.sort_direction,
      page_no: queryStringObj.page_no,
      page_size: queryStringObj.page_size
    };
  }

  downloadFileApi(method, methodUrl, data, queryString?: any) {
    if (queryString) {
      methodUrl += '?';
      for (const param of Object.keys(queryString)) {
        methodUrl += param + '=' + encodeURIComponent(queryString[param]) + '&';
      }
      // remove last '&'
      methodUrl = methodUrl.substring(0, methodUrl.length - 1);
    }
    const completeApiUrl: string = this.getCompleteApiUrl(methodUrl);
    const headerConfig: any = {
      responseType: 'blob',
      observe: 'response'
    };
    const args: Array<any> = [completeApiUrl];
    if (method.toUpperCase() === 'GET') {
      headerConfig.headers = {
        'Content-Type': 'application/json'
      };
      args.push(headerConfig);
    } else if (method.toUpperCase() === 'POST' || method.toUpperCase() === 'PUT') {
      args.push(data);
      args.push(headerConfig);
    }
    return this.httpClient[method](...args).pipe(map((res: any) => {
      const contentDisposition = res.headers.get('Content-Disposition');
      if (contentDisposition) {
        const filename = contentDisposition.substring(contentDisposition.indexOf('=') + 1);
        return {
          res,
          filename
        };
      }
      return {};
    }));
  }

  downloadFileFromResponse(response) {
    if (!response || !response.filename) {
      console.error('No file in response', response);
      return false;
    }
    const nameOfFileToDownload = response.filename;
    const blob = new Blob([response.res.body], { type: response.res.body.type });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
    } else {
      const a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = nameOfFileToDownload;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
    return true;
  }

  getPDF(url, data) {
    const completeApiUrl: string = this.getCompleteApiUrl(url);
    const pdfHttpOptions = {
      responseType: 'arraybuffer' as 'json'
    };

    return this.httpClient.post(completeApiUrl, data, pdfHttpOptions);
  }

  preaparePDF(url, data) {
    const completeApiUrl: string = this.getCompleteApiUrl(url);

    return this.httpClient.post(completeApiUrl, data, {
      observe: 'response',
      responseType: 'arraybuffer' as 'json'
    }).pipe(map((res: any) => {
      const contentDisposition = res.headers.get('Content-Disposition');
      const barcodeVal = res.headers.get('AW-Barcode');

      if (contentDisposition || barcodeVal) {
        const filename = contentDisposition.substring(contentDisposition.indexOf('=') + 1);
        return {
          res,
          filename,
          barcode: barcodeVal
        };
      }
      return {};
    }));
  }

}
