import { Injectable } from '@angular/core';
import { AuthenticationService } from 'src/app/auth/auth.service';
import * as moment from 'moment';

@Injectable()
export class SharedService {
  constructor(private authService: AuthenticationService) {
  }
  formatDate(date: string) {
    if (this.authService.getLanguage() === 'en') {
      return moment(date).format('MM-DD-YYYY');
    } else {
      return moment(date).format('MM.DD.YYYY');
    }
  }
}
