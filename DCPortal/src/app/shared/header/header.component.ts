import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debug } from 'util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  showAddUser = false;
  showEditUser = false;
  showViewUser = false;

  Routes : any;

  constructor(private _route: ActivatedRoute) { 
    this.Routes = this._route;
  }

  ngOnInit() {
    if (this.Routes && this.Routes.params) {
      this.Routes.params.subscribe(() => {
        if (this.Routes.snapshot && this.Routes.snapshot._urlSegment.segments[2]) {
          if(this.Routes.snapshot._urlSegment.segments[2] == "add"){
            this.showAddUser = true;
          }else if(this.Routes.snapshot._urlSegment.segments[2] == "list"){
            this.showViewUser = true;
          }else if(this.Routes.snapshot._urlSegment.segments[2] == "edit"){
            this.showEditUser = true;
          }
        }
      });
  }
  }
}
