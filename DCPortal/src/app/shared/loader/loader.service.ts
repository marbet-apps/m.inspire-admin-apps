import { Injectable } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Injectable()
export class LoaderService {
  isTrue: boolean;
  constructor(private spinnerService: Ng4LoadingSpinnerService) { }

  show(isTrue?: boolean) {
    if (isTrue) {
      this.spinnerService.show();
    }
  }

  hide(isTrue?: boolean) {
    if (isTrue) {
      this.spinnerService.hide();
    }
  }
}
