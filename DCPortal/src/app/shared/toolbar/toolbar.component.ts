import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cdk-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() sidenav;
  @Input() sidebar;
  @Input() drawer;
  @Input() matDrawerShow;

  searchOpen = false;
  constructor(private translate: TranslateService,
              private authenticationService: AuthenticationService,
              private router: Router) {
    translate.setDefaultLang(this.authenticationService.getLanguage());
  }

  ngOnInit() {
  }

  useLanguage(language: string) {
    this.translate.use(language);
    if (this.authenticationService.getLanguage() !== language) {
      this.authenticationService.setLanguage(language);
      this.reloadUrl();
    }
  }

  reloadUrl() {
    this.router.navigate([this.router.url]);
  }

}
