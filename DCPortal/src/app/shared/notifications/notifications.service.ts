import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { Notification, NotificationType } from '../../app.constants';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class NotificationService {
    public subject = new Subject<Notification>();
    public keepAfterRouteChange = true;

    constructor(public router: Router, private translateService: TranslateService) {
        // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterRouteChange) {
                    // only keep for a single route change
                    this.keepAfterRouteChange = false;
                } else {
                    // clear alert messages
                    this.clear();
                }
            }
        });
    }

    getAlert(): Observable<any> {
        return this.subject.asObservable();
    }

    success(message: string, islocalization = false, keepAfterRouteChange = true) {
        this.showNotification(NotificationType.Success, message, islocalization, keepAfterRouteChange);
    }

    error(message: string, islocalization = false, keepAfterRouteChange = true) {
        this.showNotification(NotificationType.Error, message, islocalization, keepAfterRouteChange);
    }

    info(message: string, islocalization = false, keepAfterRouteChange = true) {
        this.showNotification(NotificationType.Info, message, islocalization, keepAfterRouteChange);
    }

    warn(message: string, islocalization = false, keepAfterRouteChange = true) {
        this.showNotification(NotificationType.Warning, message, islocalization, keepAfterRouteChange);
    }

    showNotification(type: NotificationType, message: string, islocalization = false, keepAfterRouteChange = true) {
        this.keepAfterRouteChange = keepAfterRouteChange;
        if (!islocalization) {
            this.subject.next({ type, message } as Notification);
        } else {
            this.translateService.get(message).subscribe(res => {
                this.subject.next({ type, message: res } as Notification);
            });
        }
    }

    clear() {
        this.subject.next();
    }
}
