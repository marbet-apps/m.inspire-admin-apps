export const menus = [
    // {
    //     name: 'Home',
    //     link: '/admin/home',
    //     icon: 'dashboard',
    //     open: false,
    //     paths: 'homePaths'
    // },
    {
        name: 'UserManagement',
        link: '/admin/user/list',
        icon: 'account_circle',
        open: false,
        paths: 'userPaths'
    }, {
        name: 'RFPManagement',
        link: '/admin/rfp/list',
        icon: 'description',
        open: false,
        paths: 'rfpPaths'
    }
];

export const paths = {
    homePaths: {
        data: [
            '/admin/home',
        ],
        isActive: false
    },
    userPaths: {
        data: [
            '/admin/user/list',
            '/admin/user/add',
            '/admin/user/edit/[0-9A-Z?a-z=%&]*',
        ],
        isActive: false
    },
    rfpPaths: {
        data: [
            '/admin/rfp/list',
            '/admin/rfp/view/[0-9A-Z?a-z=%&]*',
        ],
        isActive: false
    },
};
