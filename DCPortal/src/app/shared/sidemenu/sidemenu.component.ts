import { Component, OnInit, Input } from '@angular/core';
import { menus, paths } from './menu-element';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  @Input() iconOnly = false;
  public menus = menus;
  public paths = paths;

  constructor() { }

  ngOnInit() {
  }

}
