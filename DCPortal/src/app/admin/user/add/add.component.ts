import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../shared/notifications/notifications.service';
import { AdminService } from '../../admin.service';
import { appConstants } from '../../../app.constants';
import { LoaderService } from '../../../shared/loader/loader.service';
import { MatSlideToggleChange } from '@angular/material';

@Component({
  selector: 'app-add-user',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  userId: number;
  isEdit = false;
  userDetail: any;
  countries: any[] = [];
  subscriptionTypes: any[] = [];
  navigationSubscription;
  constructor(private router: Router,
              private fb: FormBuilder,
              private notificationService: NotificationService,
              private route: ActivatedRoute,
              private adminService: AdminService,
              private loaderService: LoaderService) {
    if (this.route && this.route.params) {
      this.route.params.subscribe(params => {
        if (params.id && params.id !== '') {
          this.userId = params.id;
          this.isEdit = !!this.userId;
          this.getUserDetail();
        }
      }, err => {
        console.error(err.message);
      });
    }
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialize();
      }
    });
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      salutation: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      company: ['', [Validators.required]],
      department: ['', [Validators.required]],
      street: [''],
      postcode: [''],
      place: [''],
      land: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      telephone: [''],
      taxIdentificationNo: [''],
      subscription: ['', [Validators.required]],
      nearBy: [false],
      active: [true],
      isAdmin:[false],
      hotel: [false],
      location: [false],
      restaurant: [false],
      userMgmt: [false],
      RFPTemplate: [false],
      notes: [''],
    });

    if(this.isEdit){ 
      this.userForm.controls['email'].disable();
    }

    this.userForm.controls["isAdmin"].disable();

  }

  initialize() {
    this.getCountries();
    this.getSubscriptionTypes();
  }

  generateNewPassword() {
    const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.generatePassword;
    const dataObject = this.adminService.prepareNewPasswordRequestJson(this.userForm.value);
    this.adminService.update(apiUrl, dataObject).subscribe((res: any) => {
      this.loaderService.hide(true);
      this.notificationService.success('Users.Message.PasswordGenerated', true);
    }, (err) => {
      this.loaderService.hide(true);
    });
  }

  ValidatePermission(){
    
    if(this.userForm.controls["subscription"].value == 3){
      this.userForm.controls["hotel"].setValue(true);
      this.userForm.controls["restaurant"].setValue(true);
      this.userForm.controls["location"].setValue(true);
    }else{
      this.userForm.controls["hotel"].setValue(true);
      this.userForm.controls["restaurant"].setValue(false);
      this.userForm.controls["location"].setValue(false);
    }
  }

  getCountries() {
    const apiUrl = appConstants.api.lookup.path + appConstants.api.lookup.list.country;
    const queryParam = { page: 1, pageSize: 300, type: 1 };
    this.adminService.get(apiUrl, queryParam).subscribe((res: any) => {
      if (res && res.data && res.data.lookup && res.data.lookup.length > 0) {
        this.countries = [];
        res.data.lookup.forEach(e => {
          this.countries.push({
            value: e.Id,
            display: e.Name,
          });
        });
      }
    }, (err) => {
    });
  }
  getSubscriptionTypes() {
    const apiUrl = appConstants.api.lookup.path + appConstants.api.lookup.list.subscriptionType;
    this.adminService.get(apiUrl).subscribe((res: any) => {
      if (res && res.data && res.data.lookup && res.data.lookup.length > 0) {
        this.subscriptionTypes = [];
        res.data.lookup.forEach(e => {
          this.subscriptionTypes.push({
            value: e.Id,
            display: e.Name,
          });
        });
      }
    }, (err) => {
    });
  }

  getUserDetail() {
    this.loaderService.show(true);
    const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.getDetail;
    const queryParam = { userID: this.userId };
    this.adminService.get(apiUrl, queryParam).subscribe((res: any) => {
      if (res && res.code === appConstants.httpCodes.recordFound) {
        this.setFormValue(res);
        this.loaderService.hide(true);
      } else if (res && res.code === appConstants.httpCodes.noRecords) {
        this.notificationService.error('Users.Message.RecordNotExist', true);
        this.cancel();
      } else {
        this.notificationService.error('CommonMessage.SomethingWrong', true);
        this.cancel();
      }
    }, (err) => {
      this.loaderService.hide(true);
    });
  }

  setFormValue(res: any) {
    if (res && res.data && res.data.users && res.data.totalRecord > 0) {
      this.userDetail = res.data.users[0];
      this.userForm.patchValue(this.adminService.getUserFormDetail(this.userDetail));
    }
  }

  onSubmit() {
    if (this.userForm.valid) {
      this.loaderService.show(true);
      
      if (this.isEdit) {
        const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.updateUser;
        const dataObject = this.adminService.prepareUserUpdateRequestJson(this.userForm.value, this.userDetail);
       console.log(dataObject);
        const queryParam = { userID: this.userId, loggedInUserID: this.adminService.currentUser.LoggedInUserID || 0 };
        this.adminService.putWithParam(apiUrl, dataObject, queryParam).subscribe((res: any) => {          
          this.handleSaveResponse(res);
        }, (err) => {
          this.loaderService.hide(true);
        });
      } else {
        const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.register;
        const dataObject = this.adminService.prepareRequestJson(this.userForm.value);
        console.log(dataObject);
        this.adminService.create(apiUrl, dataObject).subscribe((res: any) => {
          this.handleSaveResponse(res);
        }, (err) => {
          this.loaderService.hide(true);
        });
      }
    }
  }

  handleSaveResponse(res: any) {
    this.loaderService.hide(true);
    if (res && ((this.isEdit && res.code === appConstants.httpCodes.userUpdated) ||
    (!this.isEdit && res.code === appConstants.httpCodes.userAdded)) ) {
      this.notificationService.success(this.isEdit ? 'Users.Message.UserUpdated' : 'Users.Message.UserAdded', true);
      this.router.navigate(['/admin/user/list']);
    } else if (res && res.code === appConstants.httpCodes.userAlreadyExist) {
      this.notificationService.error('Users.Message.UserAlreadyExist', true);
    } else {
      this.notificationService.error('Users.Message.ErrorInSave', true);
    }
  }

  cancel() {
    this.router.navigate(['/admin/user/list']);
  }

  toggleHotel(event:MatSlideToggleChange)
  {
    if(this.userForm.value.subscription<3)
    {
      if(this.userForm.value.restaurant==true)
      {       
        this.userForm.controls["restaurant"].setValue(false);              
      }

      if(this.userForm.value.location==true)
      {        
        this.userForm.controls["location"].setValue(false);       
      }      
    }
  }

  toggleLocation(event:MatSlideToggleChange)
  {
    if(this.userForm.value.subscription<3)
    {
      if(this.userForm.value.restaurant==true)
      {       
        this.userForm.controls["restaurant"].setValue(false);              
      }

      if(this.userForm.value.hotel==true)
      {
         this.userForm.controls["hotel"].setValue(false);            
      }     
    }
  }

  toggleRestuarant(event:MatSlideToggleChange)
  {
    if(this.userForm.value.subscription<3)
    {
      if(this.userForm.value.hotel==true)
      {       
        this.userForm.controls["hotel"].setValue(false);              
      }

      if(this.userForm.value.location==true)
      {       
        this.userForm.controls["location"].setValue(false);       
      }      
    }
  }




}
