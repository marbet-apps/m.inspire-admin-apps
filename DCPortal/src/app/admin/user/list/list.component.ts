import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {FormControl} from '@angular/forms';
// import { SelectionModel } from '@angular/cdk/collections';
import { LoaderService } from '../../../shared/loader/loader.service';
import { TableAction, appConstants, ActiveStatus } from 'src/app/app.constants';
import { Router } from '@angular/router';
import { AdminService } from '../../admin.service';
import { NotificationService } from '../../../shared/notifications/notifications.service';
import { User } from './user';
import { tap, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list-user',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListUserComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  // displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol']; //with checkbox
  displayedColumns: string[] = ['First_Name','Last_Name','Company_Name', 'Email', 'IsActive', 'action'];
  dataSource: MatTableDataSource<User>;
  // selection = new SelectionModel<User>(true, []);
  defaultPageIndex = 0;
  defaultPageSize = 10;
  totalRecords: number;
  queryParam: any;

  activeStatusControl = new FormControl('All');

  constructor(private loaderService: LoaderService,private translateService: TranslateService, private router: Router, private adminService: AdminService,
              private notificationService: NotificationService) {
              }


  ngOnInit() {
    this.queryParam = {
      email: '',
      activestatus: 'All',
      page: this.defaultPageIndex,
      pageSize: this.defaultPageSize
    };
    this.getList();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // server-side search
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        filter((e: KeyboardEvent) => e.keyCode === 13),
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getList();
        })
      )
      .subscribe();


    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.getList())
      )
      .subscribe();
  }

  tableAction(id: any, operation: string) {
    if (operation === TableAction.View) {
      this.router.navigate(['/admin/user/edit', id]);
    } else if (operation === TableAction.Delete) {
      this.translateService.get("Users.Message.DeleteConfirm").subscribe(res => { 
        if(confirm(res)) {
          this.deleteUser(id);
        }
      });
     
    }
  }
  

  deleteUser(id: any) {
    const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.deleteUser;
    const queryParam = { userID: id };
    this.adminService.deleteWithQueryParams(apiUrl, {}, queryParam).subscribe((res: any) => {
      this.loaderService.hide(true);
      this.getList();
      if(res != null && res.code == '702'){
        this.notificationService.success('Users.Message.UserDeleted', true);
      }else{
        this.notificationService.error('Users.Message.UserDeletedFailed', true);
      }
    }, (err) => {
      this.loaderService.hide(true);
    });
  }

  addUser() {
    this.router.navigate(['/admin/user/add']);
  }

  filterUser() {
    this.getList();
  }

  getList() {
  this.loaderService.show(true);
  this.queryParam = {};
  if (this.filter && this.filter.nativeElement && this.filter.nativeElement.value) {
    this.queryParam.email = this.filter.nativeElement.value;
  }
  this.queryParam.page = this.paginator.pageIndex + 1;
  this.queryParam.pageSize = this.paginator.pageSize || this.defaultPageSize;
  this.queryParam.activestatus = this.activeStatusControl.value;

  const apiUrl = appConstants.api.admin.path + appConstants.api.admin.users.getAll;
  this.adminService.list(apiUrl, 'GET', this.queryParam).subscribe((response: any) => {
    this.displayListOfUsers(response);
    this.loaderService.hide(true);
  }, (err) => {
    this.dataSource = new MatTableDataSource<User>();
    this.totalRecords = 0;
    this.loaderService.hide(true);
    console.error('Error while getting user list:', err);
    if (err && err.code === appConstants.httpCodes.insufficient_param) {
      this.notificationService.warn('CommonMessage.InvalidParameters', true);
    } else {
      this.notificationService.warn('CommonMessage.SomethingWrong', true);
    }
  });
  }

  displayListOfUsers(response: any) {
    if (response && response.data && response.data.users && response.data.totalRecord > 0) {
      let list = [];
      list = response.data.users;
      this.totalRecords = response.data.totalRecord;
      this.dataSource = new MatTableDataSource<User>(list.map(u => {
        return {
          Userid: u.Userid,
          First_Name:u.First_Name,
          Last_Name:u.Last_Name,
          Company_Name: u.Company_Name,
          Email: u.Email,
          IsActive: u.IsActive ? ActiveStatus.Active : ActiveStatus.InActive
        } as User;
      }));
    } else {
      this.dataSource = new MatTableDataSource<User>();
      this.totalRecords = 0;
    }
  }

  // with checkbox
  // /** Whether the number of selected elements matches the total number of rows. */
  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource.data.length;
  //   return numSelected === numRows;

  // }

  // /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle() {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.data.forEach(row => this.selection.select(row));
  // }
  // /** The label for the checkbox on the passed row */
  // checkboxLabel(row?: PeriodicElement): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //   }
  //   return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  // }

}
