export interface User {
    Userid: number;
    First_Name:string;
    Last_Name:string;
    Company_Name: string;
    Email: string;
    IsActive: string;
}
