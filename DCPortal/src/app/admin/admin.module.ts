import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from '../admin/admin.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AdminService } from './admin.service';
import { HomeComponent } from './home/home.component';
import { AddUserComponent } from './user/add/add.component';
import { ListUserComponent } from './user/list/list.component';
import { ListRFPComponent } from './rfp/list/list.component';
import { ViewRFPComponent } from './rfp/view/view.component';
@NgModule({
  declarations: [
    AdminComponent,
    ChangePasswordComponent,
    EditProfileComponent,
    HomeComponent,
    AddUserComponent,
    ListUserComponent,
    ListRFPComponent,
    ViewRFPComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ],
  providers: [
    AdminService
  ]
})
export class AdminModule { }
