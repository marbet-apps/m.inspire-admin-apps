import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRFPComponent } from './view.component';

describe('ViewRFPComponent', () => {
  let component: ViewRFPComponent;
  let fixture: ComponentFixture<ViewRFPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRFPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRFPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
