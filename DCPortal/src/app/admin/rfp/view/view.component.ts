import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-view-rfp',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewRFPComponent {

  constructor(
    public dialogRef: MatDialogRef<ViewRFPComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}