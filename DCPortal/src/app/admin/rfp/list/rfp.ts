export interface RFP {
    RfpId: number;
    UserName: string;
    Event_Name: string;
    CreatedDate: string;
    DaysRunning: string;
    Hotel_Status: string;
    TotalHotelResource: string;
    Restaurant_Status:string;
    TotalRestaurantResource:string;
    Location_Status:string;
    TotalLocationResource:string;
}
