import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormControl } from '@angular/forms';
import { LoaderService } from '../../../shared/loader/loader.service';
import { TableAction, appConstants, ActiveStatus } from 'src/app/app.constants';
import { Router, NavigationEnd } from '@angular/router';
import { AdminService } from '../../admin.service';
import { NotificationService } from '../../../shared/notifications/notifications.service';
import { RFP } from './rfp';
import { tap, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { fromEvent, merge } from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewRFPComponent } from '../view/view.component';

@Component({
  selector: 'app-list-rfp',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListRFPComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  displayedColumns: string[] = ['UserName','Event_Name', 'CreatedDate', 'DaysRunning', 'Hotel_Status', 'Restaurant_Status', 'Location_Status'];
  dataSource: MatTableDataSource<RFP>;
  defaultPageIndex = 0;
  defaultPageSize = 10;
  totalRecords: number;
  queryParam: any;
  requestStatus: any[] = [];

  activeStatusControl = new FormControl('All');

  navigationSubscription;

  constructor(public dialog: MatDialog,private loaderService: LoaderService, private router: Router, private adminService: AdminService,
              private notificationService: NotificationService, private sharedService: SharedService) {

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialize();
      }
    });
  }


  ngOnInit() {
    // this.initialize();
  }

  initialize() {
    this.queryParam = {
      term: '',
      status: 'All',
      page: this.defaultPageIndex,
      pageSize: this.defaultPageSize
    };
    this.getRequestStatus();
    this.getList();
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // server-side search
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        filter((e: KeyboardEvent) => e.keyCode === 13),
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.getList();
        })
      )
      .subscribe();


    // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.getList())
      )
      .subscribe();
  }

  tableAction(id: any,type:string) {
    const dialogRef = this.dialog.open(ViewRFPComponent, {
      width: '80%',
      data: {name: "TEst", animal: "Test!23"}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  filterRFP() {
    this.getList();
  }

  getRequestStatus() {
    const apiUrl = appConstants.api.lookup.path + appConstants.api.lookup.list.requestStatus;
    this.adminService.get(apiUrl).subscribe((res: any) => {
      if (res && res.data && res.data.lookup && res.data.lookup.length > 0) {
        this.requestStatus = [];
        res.data.lookup.forEach(e => {
          this.requestStatus.push({
            value: e.Id,
            display: e.Name,
          });
        });
      }
    }, (err) => {
    });
  }

  getList() {
    this.loaderService.show(true);
    this.queryParam = {};
  
    if (this.filter && this.filter.nativeElement && this.filter.nativeElement.value) {
      this.queryParam.search = this.filter.nativeElement.value;
    }
    this.queryParam.page = this.paginator.pageIndex + 1;
    this.queryParam.pageSize = this.paginator.pageSize || this.defaultPageSize;
    this.queryParam.status = this.activeStatusControl.value;

    const apiUrl = appConstants.api.admin.path + appConstants.api.admin.rfp.getRFP;
    
    this.adminService.list(apiUrl, 'GET', this.queryParam).subscribe((response: any) => {
      this.displayListOfRFP(response);
      this.loaderService.hide(true);
    }, (err) => {
      this.dataSource = new MatTableDataSource<RFP>();
      this.totalRecords = 0;
      this.loaderService.hide(true);
      console.error('Error while getting rfp list:', err);
      if (err && err.code === appConstants.httpCodes.insufficient_param) {
        this.notificationService.warn('CommonMessage.InvalidParameters', true);
      } else {
        this.notificationService.warn('CommonMessage.SomethingWrong', true);
      }
    });
  }

  displayListOfRFP(response: any) {
    if (response && response.data && response.data.rfpmicerequestwithproposalmodel && response.data.totalrecords > 0) {
      let list = [];
      list = response.data.rfpmicerequestwithproposalmodel;
      this.totalRecords = response.data.totalrecords;
      this.dataSource = new MatTableDataSource<RFP>(list.map(u => {
        return {
          RfpId: u.rfp_id,
          UserName: u.username,
          Event_Name: u.event_name,
          CreatedDate: u.created_date,
          DaysRunning: u.days_running,
          Hotel_Status: u.hotel_Status,
          TotalHotelResource: u.totalhotelresource,
          Restaurant_Status:u.restaurant_Status,
          TotalRestaurantResource:u.totalrestaurantresource,
          Location_Status:u.location_Status,
          TotalLocationResource:u.totallocationresource
        } as RFP;
      }));
    } else {
      this.dataSource = new MatTableDataSource<RFP>();
      this.totalRecords = 0;
    }
  }

}
