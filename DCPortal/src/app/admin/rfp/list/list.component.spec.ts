import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRFPComponent } from './list.component';

describe('ListRFPComponent', () => {
  let component: ListRFPComponent;
  let fixture: ComponentFixture<ListRFPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListRFPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRFPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
