import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { AdminComponent } from '../admin/admin.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { HomeComponent } from './home/home.component';
import { AddUserComponent } from './user/add/add.component';
import { ListUserComponent } from './user/list/list.component';
import { ListRFPComponent } from './rfp/list/list.component';
import { ViewRFPComponent } from './rfp/view/view.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: AdminComponent,
    children: [
      {
        path: 'user',
        children: [
          {
            path: 'list',
            component: ListUserComponent
          },
          {
            path: 'add',
            component: AddUserComponent
          },
          {
            path: 'edit/:id',
            component: AddUserComponent
          },
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: '**',
            redirectTo: 'list'
          }
        ]
      },
      {
        path: 'rfp',
        children: [
          {
            path: 'list',
            component: ListRFPComponent
          },
          {
            path: 'view/:id',
            component: ViewRFPComponent
          },
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          },
          {
            path: '**',
            redirectTo: 'list'
          }
        ]
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'edit-profile',
        component: EditProfileComponent
      },
      {
        path: 'user',
        component: HomeComponent
      },
      {
        path: '',
        redirectTo: 'user',
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: 'user'
      }]
  }
];
// const routes: Routes = [
//   {
//     path: '',
//     canActivate: [AuthGuard],
//     component: ConsumerComponent,
//     children: [
//       {
//         path: 'user',
//         children: [
//           {
//             path: 'list',
//             component: UserslistComponent
//           },
//           {
//             path: 'add',
//             component: UsersComponent
//           },
//           {
//             path: 'edit',
//             component: UsersComponent
//           }
//         ]
//       },
//       {
//         path: 'changepassword',
//         component: ChangepasswordComponent
//       },
//       {
//         path: 'editprofile',
//         component: EditprofileComponent
//       },
//       {
//         path: 'index',
//         component: HomeComponent
//       },
//       {
//         path: '',
//         redirectTo: 'index',
//         pathMatch: 'full'
//       },
//       {
//         path: '**',
//         redirectTo: 'index'
//       }]
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule { }
