import { Injectable } from '@angular/core';
import { HttpBaseService } from '../shared/services/httpbase.service';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';
import { AuthenticationService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminService extends HttpBaseService {
  currentUser: any;
  constructor(httpClient: HttpClient, private authService: AuthenticationService) {
    super(httpClient);
    this.currentUser = this.authService.currentUser;
  }

  prepareRequestJson(dataObj: any) {
    return {
      Salutation: dataObj.salutation,
      First_Name: dataObj.firstName,
      Last_Name: dataObj.lastName,
      Email: dataObj.email,
      Company_Name: dataObj.company,
      Department_Name: dataObj.department,
      IsActive: dataObj.active,
      Street: dataObj.street,
      PostCode: dataObj.postcode,
      Place: dataObj.place,
      Telephone: dataObj.telephone,
      Tax_Indentifier_No: dataObj.taxIdentificationNo,
      Country_Id: dataObj.land,
      Hotel: dataObj.hotel,
      Location: dataObj.location,
      Restaurant: dataObj.restaurant,
      UserMgmt: dataObj.userMgmt,
      RFPTemplate: dataObj.RFPTemplate,
      Subscription_Type_Name: dataObj.subscription,
      NearBy: dataObj.nearBy,
      Isself_Created: true,
      IsApproved: true,
      IsDeleted: false,
      GoogleMap: true,
    };
  }

  prepareUserUpdateRequestJson(dataObj: any, originalObj: any) {
    return _.merge({}, originalObj, {
      Salutation: dataObj.salutation,
      First_Name: dataObj.firstName,
      Last_Name: dataObj.lastName,
      Email: dataObj.email,
      Company_Name: dataObj.company,
      Department_Name: dataObj.department,
      IsActive: dataObj.active,
      Street: dataObj.street,
      PostCode: dataObj.postcode,
      Place: dataObj.place,
      Telephone: dataObj.telephone,
      Tax_Indentifier_No: dataObj.taxIdentificationNo,
      Country_Id: dataObj.land,
      Hotel: dataObj.hotel,
      Location: dataObj.location,
      Restaurant: dataObj.restaurant,
      UserMgmt: dataObj.userMgmt,
      RFPTemplate: dataObj.RFPTemplate,
      Subscription_Type_Name: dataObj.subscription,
      isAdmin:dataObj.subscription == 3 ?true:false,
      NearBy: dataObj.nearBy,
    });
  }

  getUserFormDetail(userDetail: any) {
    return {
      salutation: userDetail.Salutation,
      firstName: userDetail.First_Name,
      lastName: userDetail.Last_Name,
      company: userDetail.Company_Name,
      department: userDetail.Department_Name,
      street: userDetail.Street,
      postcode: userDetail.PostCode,
      place: userDetail.Place,
      land: userDetail.Country_Id,
      email: userDetail.Email,
      telephone: userDetail.Telephone,
      taxIdentificationNo: userDetail.Tax_Indentifier_No,
      active: userDetail.IsActive,
      hotel: userDetail.Hotel,
      location: userDetail.Location,
      restaurant: userDetail.Restaurant,
      userMgmt: userDetail.UserMgmt,
      RFPTemplate: userDetail.RFPTemplate,
      subscription: userDetail.Subscription_Type_Name,
      isAdmin:userDetail.isAdmin,
      nearBy: userDetail.NearBy,
    };
  }

  prepareSearchRequestJson(dataObj: any) {
    if (!dataObj) {
      dataObj = {};
    }
  }

  prepareNewPasswordRequestJson(dataObj: any) {
    return {
      Email: dataObj.email,
      Password: dataObj.password,
    };
  }

}
